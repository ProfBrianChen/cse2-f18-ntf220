// Nick Facendo, 9/6/18, cse2. Purposes of program: print the number of minutes for each trip,
//print the number of counts for each trip,
//print the distance of each trip in miles,
//print the distance for the two trips combined.
public class Cyclometer {
  //main method
  public static void main(String[] args) {

    int secsTrip1=480;  // time of trip 1 in seconds
   	int secsTrip2=3220;  // time of trip 2 in seconds 
		int countsTrip1=1561;  // how many rotations in trip 1
		int countsTrip2=9037; // how many rotations in trip 2  
    double wheelDiameter=27.0; // variable for useful constant 
  	double PI=3.14159; // variable for useful constant 
  	int feetPerMile=5280; // variable for useful constant 
  	int inchesPerFoot=12; // variable for useful constant 
  	int secondsPerMinute=60; // variable for useful constant 
	  double distanceTrip1,distanceTrip2,totalDistance; // variable for useful constant 
    System.out.println(" Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); // converting seconds to minutes 
	  System.out.println(" Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); // converting seconds to minutes 
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); // output data 
	  System.out.println("Trip 2 was "+distanceTrip2+" miles"); // output data
	  System.out.println("The total distance was "+totalDistance+" miles"); // output data
    } // end of main method 
  } // end of class 